<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            'name' => 'Чистый код',
            'author' => 'Роберт Мартин',
            'rare' => '0',
            'count_book' => 3,
            'cost' => '357',
        ]);
        DB::table('books')->insert([
            'name' => 'Совершенный код',
            'author' => 'Стив Макконнелл',
            'rare' => '0',
            'count_book' => 3,
            'cost' => '535',
        ]);
        DB::table('books')->insert([
            'name' => 'Язык программирования С',
            'author' => 'Брайан Керниган, Деннис Ритчи',
            'rare' => '1',
            'count_book' => 1,
            'cost' => '470',
        ]);
        DB::table('books')->insert([
            'name' => 'Java. Эффективное программирование',
            'author' => 'Джошуа Блох',
            'rare' => '0',
            'count_book' => 4,
            'cost' => '680',
        ]);
        DB::table('books')->insert([
            'name' => 'Изучаем Java',
            'author' => 'Кэти Сьерра и Берт Бейтс',
            'rare' => '1',
            'count_book' => 2,
            'cost' => '480',
        ]);
    }
}
