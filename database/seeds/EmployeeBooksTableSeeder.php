<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeeBooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employee_books')->insert([
            'user_id' => 1,
            'book_id' => 1,
            'date_issue' => '2017-04-05',
            'day_count' => 7,
        ]);
        DB::table('employee_books')->insert([
            'user_id' => 1,
            'book_id' => 2,
            'date_issue' => '2017-04-05',
            'day_count' => 7,
        ]);
    }
}
