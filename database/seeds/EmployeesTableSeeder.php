<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert([
            'shifr' => '200501',
            'fam' => 'Иванов',
            'name' => 'Иван',
            'otch' => 'Иванович',
        ]);

        DB::table('employees')->insert([
            'shifr' => '312312',
            'fam' => 'Петрова',
            'name' => 'Ирина',
            'otch' => 'Алексеевна',
        ]);

        DB::table('employees')->insert([
            'shifr' => '785445',
            'fam' => 'Сидоров',
            'name' => 'Лев',
            'otch' => 'Александрович',
        ]);
    }
}
