<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    protected $table = 'employees';

    protected $fillable = [
        'shifr', 'fam', 'name', 'otch',
    ];

    public function books()
    {
        return $this->belongsToMany('App\Books', 'employee_books', 'user_id', 'book_id');
    }
}
