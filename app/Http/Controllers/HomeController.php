<?php

namespace App\Http\Controllers;

use App\Books;
use App\Employees;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employ = Employees::All();
        return view('home', compact('employ'));
    }

    public function books()
    {
        $books = Books::All();
        return view('books', compact('books'));
    }

    public function employees()
    {
        $books = Books::All();
        return view('books', compact('books'));
    }

    public function addBooks(Request $request)
    {
        $books = new Books();
        $books->name = $request->post('name');
        $books->author = $request->post('author');
        $books->rare = $request->post('rate');
        $books->cost = $request->post('cost');
        $books->count_book = $request->post('count_book');
        $books->save();
        return redirect('/books');
    }

    public function editBook(Request $request)
    {
        $books = Books::find($request->post('id'));
        $books->name = $request->post('name');
        $books->author = $request->post('author');
        $books->rare = $request->post('rate');
        $books->cost = $request->post('cost');
        $books->count_book = $request->post('count_book');
        $books->save();
        return redirect('/books');
    }

    public function deleteBooks($id)
    {
        $books = Books::find($id);
//        DB::table('books')->where('id', '=', $request->post('id'))->delete();
        $books -> delete();
        return redirect('/books');
    }

    public function editBooks($id)
    {
        $books = Books::findOrFail($id);
        return view('editBook', ['books' => $books]);
    }
}
