<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeBook extends Model
{
    protected $table = 'employee_books';

    protected $fillable = [
        'user_id', 'book_id', 'date_issue', 'day_count',
    ];

}
