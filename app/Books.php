<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    protected $table = 'books';

    protected $fillable = [
        'name', 'author', 'rare', 'cost', 'count_book'
    ];

    public function employees()
    {
        return $this->belongsToMany('App\Employees', 'employee_books', 'book_id', 'user_id');
    }
}
