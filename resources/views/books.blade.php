@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <nav class="navbar navbar-expand-lg navbar-light bg-light ">
                            <div class="collapse navbar-collapse justify-content-between" id="navbarNav">
                                <ul class="navbar-nav">
                                    <li class="nav-item">
                                        <a class="nav-link" href="/home">Главная</a>
                                    </li>
                                    <li class="nav-item active">
                                        <a class="nav-link" href="/books">Книги</a>
                                    </li>
                                </ul>
                                <div class="navbar-nav">
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
                                        Новая книга
                                    </button>
                                </div>
                            </div>
                        </nav>
                    </div>

                    <div class="card-body">
                        <table class="table">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">Наименование</th>
                                <th  scope="col">Автор</th>
                                <th class="text-center" scope="col">Признак редкости</th>
                                <th class="text-center" scope="col">Количество</th>
                                <th class="text-center" scope="col">Стоимость, руб.</th>
                                <th class="text-center" scope="col">Функционал</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($books as $empl_cl)
                                <tr>
                                    <th>{{$empl_cl->name}}</th>
                                    <td>{{$empl_cl->author}}</td>
                                    @if($empl_cl->rare == true)
                                    <td class="text-center">Редкая</td>
                                    @else
                                        <td class="text-center">Обычный экземпляр</td>
                                    @endif
                                    <td class="text-center">{{$empl_cl->count_book}}</td>
                                    <td class="text-center">{{$empl_cl->cost}}</td>
                                    <td class="text-center">
                                        <a style="margin-bottom: 5px" class="btn btn-primary" href="/books/edit/{{$empl_cl->id}}">
                                           Редактировать
                                        </a>
                                        <a href="/books/delete/{{$empl_cl->id}}" class="btn btn-danger" >
                                            Удалить
                                        </a>
                                    </td>
                                </tr>

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Новая книга</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/books/add" method="post" data-toggle="validator" role="form">
                    {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group col-md-12">
                        <label for="inputEmail4">Наименование</label>
                        <input type="text" class="form-control" id="inputEmail4" name="name" placeholder="Наименование" required>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="inputPassword4">Автор</label>
                        <input type="text" class="form-control" id="inputPassword4" name="author" placeholder="Автор" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputPassword12">Признак редкости</label>
                        <select class="form-control" id="inputPassword12" name="rate">
                            <option value="0">Обычный экземпляр</option>
                            <option value="1">Редкая</option>
                        </select>
                    </div>
                    <div class="row" style="margin-left: 3px;">
                        <div class="form-group col-md-5">
                            <label for="inputEmai2">Количество</label>
                            <input type="text" class="form-control" id="inputEmai2" name="count_book" placeholder="Количество" required>
                        </div>
                        <div class="form-group col-md-5">
                            <label for="inputPassword43">Стоимость, рублей</label>
                            <input type="text" class="form-control" id="inputPassword43" name="cost" placeholder="Стоимость" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
