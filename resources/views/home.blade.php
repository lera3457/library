@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#">Главная</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/books">Книги</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Шифр</th>
                            <th scope="col">Фамилия</th>
                            <th scope="col">Имя</th>
                            <th scope="col">Отчество</th>
                            <th scope="col">Набор книг</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($employ as $empl_cl)
                            @if(count($empl_cl->books) > 0)
                            <tr>
                                <th>{{$empl_cl->shifr}}</th>
                                <td>{{$empl_cl->fam}}</td>
                                <td>{{$empl_cl->name}}</td>
                                <td>{{$empl_cl->otch}}</td>
                                <td>@foreach($empl_cl->books as $book)
                                {{$book->name}} - {{$book->author}} <br>
                                @endforeach</td>
                            </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
