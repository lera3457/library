@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <nav class="navbar navbar-expand-lg navbar-light bg-light ">
                            <div class="collapse navbar-collapse justify-content-between" id="navbarNav">
                                <ul class="navbar-nav">
                                    <li class="nav-item">
                                        <a class="nav-link" href="/home">Главная</a>
                                    </li>
                                    <li class="nav-item active">
                                        <a class="nav-link" href="/books">Книги</a>
                                    </li>
                                </ul>
                                <div class="navbar-nav">
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
                                        Новая книга
                                    </button>
                                </div>
                            </div>
                        </nav>
                    </div>

                    <div class="card-body">
                        <form action="/books/editBook" method="post" data-toggle="validator" role="form">
                            {{ csrf_field() }}
                            <div class="modal-body">
                                <div class="form-group col-md-12">
                                    <label for="inputEmail4">Наименование</label>
                                    <input type="text" class="form-control" id="inputEmail4" value="{{$books->name}}" name="name" placeholder="Наименование" required>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="inputPassword4">Автор</label>
                                    <input type="text" class="form-control" id="inputPassword4" value="{{$books->author}}" name="author" placeholder="Автор" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputPassword12">Признак редкости</label>
                                    <select class="form-control" id="inputPassword12" name="rate">
                                        @if($books->rare == true) <option value="1" selected>Редкая</option><option value="0">Обычный экземпляр</option>
                                        @else <option value="1" >Редкая</option><option selected value="0">Обычный экземпляр</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="row" style="margin-left: 3px;">
                                    <div class="form-group col-md-5">
                                        <label for="inputEmai2">Количество</label>
                                        <input type="text" class="form-control" id="inputEmai2" value="{{$books->count_book}}" name="count_book" placeholder="Количество" required>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="inputPassword43">Стоимость, рублей</label>
                                        <input type="text" class="form-control" id="inputPassword43" value="{{$books->cost}}" name="cost" placeholder="Стоимость" required>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="id" value="{{$books->id}}">
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
