<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/books', 'HomeController@books')->name('books');
Route::get('/employees', 'HomeController@employees')->name('employees');
Route::post('/books/add', 'HomeController@addBooks')->name('addBooks');
Route::get('/books/edit/{id}', 'HomeController@editBooks')->name('editBooks');
Route::get('/books/delete/{id}', 'HomeController@deleteBooks')->name('deleteBooks');
Route::post('/books/editBook', 'HomeController@editBook')->name('editBook');